Feature: Intégration des précandidats en éxistant déjà et n'ayant pas changé de formation

  Scenario: Intégration d'un précandidat - MAJ nom
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode |
      | 100        | 11       | john | S       |
    When Lancer le job integration parcoursup
    Then Il y a 1 precandidat(s) dans la base precandidat
    Then Le precandidat 100 s'appelle john
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        | 11       | jack | S       | 100          |
    When Lancer le job integration parcoursup
    Then Il y a 1 precandidat(s) dans la base precandidat
    Then Le precandidat 100 s'appelle jack

  Scenario: Intégration d'un précandidat - Changement diplôme inconnu
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        | 11       | john | S       | 100          |
    When Lancer le job integration parcoursup
    Then Il y a 1 precandidat(s) dans la base precandidat
    Then Le precandidat 100 s'appelle john
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        | 12       | jack | S       | 100          |
    When Lancer le job integration parcoursup
    Then Il y a 1 precandidat(s) dans la base precandidat
    Then Le precandidat 100 s'appelle john
    Then Admis 100 est en rejet CODE_DIPLOME_INCONNU