package nc.unc.importparcoursup.io;

import nc.unc.importparcoursup.batch.io.admis.AdmisRepoReader;
import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisPageableRepository;
import nc.unc.importparcoursup.helper.ModelTestHelper;
import nc.unc.importparcoursup.model.UAI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AdmisRepoReaderTest {

    @Autowired
    AdmisPageableRepository admisPageableRepo;

    @Autowired
    AdmisHistoryRepository historyRepo;

    @Test
    public void testAdmisRepoReader() throws Exception {
        ModelTestHelper modelHelper = new ModelTestHelper();
        historyRepo.saveAll(List.of(modelHelper.createAdmisHistory(modelHelper.today()), modelHelper.createAdmisHistory(modelHelper.yesterday())));

        // retrieve admis history
        AdmisHistory historyToday = historyRepo.findById(1L).get();
        AdmisHistory historyYesterday = historyRepo.findById(2L).get();

        // Vérification historyAdmis
        assertThat(modelHelper.today()).isEqualTo(historyToday.getJobExecutionDate());
        assertThat(modelHelper.yesterday()).isEqualTo(historyYesterday.getJobExecutionDate());

        // create admis
        Admis stuToday1 = modelHelper.createAdmis(historyToday);
        Admis stuToday2 = modelHelper.createAdmis(historyToday);
        Admis stuYesterday = modelHelper.createAdmis(historyYesterday);
        admisPageableRepo.saveAll(List.of(stuToday1, stuToday2, stuYesterday));

        AdmisRepoReader readerToday = new AdmisRepoReader(admisPageableRepo, historyToday);
        AdmisRepoReader readerYesterday = new AdmisRepoReader(admisPageableRepo, historyYesterday);

        // 2 admis today
        assertThat(modelHelper.today()).isEqualTo(readerToday.read().getAdmisHistory().getJobExecutionDate());
        assertThat(modelHelper.today()).isEqualTo(readerToday.read().getAdmisHistory().getJobExecutionDate());
        assertNull(readerToday.read());

        // 1 admis yesterday
        assertThat(modelHelper.yesterday()).isEqualTo(readerYesterday.read().getAdmisHistory().getJobExecutionDate());
        assertNull(readerYesterday.read());
    }

    @Test
    public void testCheckSum() {
        String CHECK_SUM = "checksum123";
        ModelTestHelper modelHelper = new ModelTestHelper();
        AdmisHistory histo1 = modelHelper.createAdmisHistory(1L, modelHelper.today());
        histo1.setFileCheckSum(CHECK_SUM);
        historyRepo.save(histo1);

        assertThat(historyRepo.findFirstByFileCheckSum(CHECK_SUM)).isNotNull();
        assertThat(historyRepo.findFirstByFileCheckSum(CHECK_SUM + "!")).isNull();
    }

    @Test
    public void testCheckLastByUAI() {


        ModelTestHelper modelHelper = new ModelTestHelper();
        AdmisHistory histoToday = modelHelper.createAdmisHistory(1L, modelHelper.today());
        histoToday.setUai(UAI.IUT);
        AdmisHistory histoYesterday = modelHelper.createAdmisHistory(2L, modelHelper.yesterday());
        histoYesterday.setUai(UAI.IUT);
        AdmisHistory histoOther = modelHelper.createAdmisHistory(3L, modelHelper.yesterday());
        histoYesterday.setUai(UAI.UNIV);

        historyRepo.saveAll(List.of(histoToday, histoYesterday, histoOther));
        AdmisHistory find = historyRepo.findTopByUaiOrderByJobExecutionDateDesc(UAI.IUT);
        assertThat(find.getId()).isEqualTo(histoToday.getId());
        assertThat(find.getUai()).isEqualTo(histoToday.getUai());
    }


    @Test
    public void testFindLastHistory() {
        ModelTestHelper modelHelper = new ModelTestHelper();
        historyRepo.saveAll(List.of(modelHelper.createAdmisHistory(modelHelper.yesterday()), modelHelper.createAdmisHistory(modelHelper.today()), modelHelper.createAdmisHistory(modelHelper.yesterday())));
        assertThat(modelHelper.today()).isEqualTo(historyRepo.findTopByOrderByJobExecutionDateDesc().getJobExecutionDate());
    }


}
