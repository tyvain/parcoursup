package nc.unc.importparcoursup.io;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.AdmisRejet;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatPageableRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidature;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatureRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CandidatRepositoryTest {
    @Autowired
    PreCandidatureRepository preCandidatureRepository;
    @Autowired
    PreCandidatRepository preCandidatRepository;
    @Autowired
    AdmisHistoryRepository admisHistoryRepository;
    @Autowired
    AdmisRejetRepository admisRejetRepository;
    @Autowired
    AdmisRepository admisRepository;
    @Autowired
    PreCandidatPageableRepository candidatPageableRepository;


    @Test
    public void admisPageableRepositoryTest(){
        preCandidatRepository.save(new PreCandidat(1L));
        preCandidatRepository.save(new PreCandidat(2L));
        assertThat(candidatPageableRepository
                .findAll(PageRequest.of(1,10)).getTotalElements()).isEqualTo(2);
    }

    @Test
    public void admiHistoryRepositoryTest() {
        Admis admis = new Admis();
        AdmisHistory admisHistory = new AdmisHistory();
        AdmisRejet admisRejet = new AdmisRejet();
        admis.setAdmisHistory(admisHistory);
        admisRejet.setAdmis(admis);
        admis.setAdmisRejet(List.of(admisRejet));
        admisHistoryRepository.save(admisHistory);
        admisRepository.save(admis);
        assertThat(admisRepository.findAllAdmisByAdmisHistory(admisHistory).stream()
                .filter(adm -> adm.getAdmisRejet() != null)
                .count()).isEqualTo(1);
    }

    @Test
    public void admisRejetRepositoryTest() {
        Admis admis = new Admis();
        AdmisHistory admisHistory = new AdmisHistory();
        AdmisRejet admisRejet = new AdmisRejet();
        admis.setAdmisHistory(admisHistory);
        admisRejet.setAdmis(admis);
        admis.setAdmisRejet(List.of(admisRejet));
        admisRepository.save(admis);
        admisRejetRepository.save(admisRejet);
        admisHistoryRepository.save(admisHistory);
        assertThat(admisHistoryRepository.count()).isEqualTo(1);
        assertThat(admisRejetRepository.count()).isEqualTo(1);
        assertThat(admisRepository.count()).isEqualTo(1);
        assertThat(admisRejetRepository.findAdmisRejetByAdmisHistory(admisHistory).size()).isEqualTo(1);
    }

    @Test
    public void preCandidatureRepositoryTest() {
        PreCandidat preCandidat = new PreCandidat();
        preCandidat.setCAND_KEY(123L);
        PreCandidature preCandidature = new PreCandidature();
        preCandidature.setCANU_KEY(345L);
        preCandidature.setPreCandidat(preCandidat);
        preCandidatureRepository.save(preCandidature);
        assertThat(preCandidatureRepository.count()).isEqualTo(1);
        assertThat(preCandidatRepository.count()).isEqualTo(1);
    }

    @Test
    public void preCandidatRepositoryTest() {
        PreCandidature preCandidature = new PreCandidature();
        PreCandidat preCandidat = new PreCandidat();
        preCandidat.setPreCandidature(preCandidature);
        PreCandidat preCandidatSaved = preCandidatRepository.save(preCandidat);
        PreCandidat preCandidatResultat = preCandidatRepository.findById(preCandidatSaved.getCAND_KEY()).get();
        assertThat(preCandidatRepository.count()).isEqualTo(1);
        assertThat(preCandidatureRepository.count()).isEqualTo(1);
        assertThat(preCandidatResultat.getPreCandidature()).isNotNull();
    }
}


