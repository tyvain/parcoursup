package nc.unc.importparcoursup.utils;

import nc.unc.importparcoursup.dao.admisDAO.AdmisRejet;
import nc.unc.importparcoursup.model.AdmisSkipException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MapperUtils {
    private static final Logger log = LoggerFactory.getLogger(MapperUtils.class);

    public static final String UTF8_BOM = "\uFEFF";

    public static Date stringToDate(String s) {
        try {
            return new SimpleDateFormat("ddMMYYYY").parse(s);
        } catch (ParseException e) {
            log.error("Can't parse date " + s);
            return null;
        }
    }

    public static long stringToLong(String s) {
        if (s != null) {
            return Long.parseLong(removeUTF8BOM(s));
        }
        return 0L;
    }

    public static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    public static AdmisRejet getAdmisRejetFromAdmisSkipException(AdmisSkipException ex) {
        AdmisRejet rejet = new AdmisRejet();
        rejet.setAdmis(ex.getAdmis());
        rejet.setTypeRejet(ex.getTypeRejet());
        rejet.setInformation(ex.getInformation());
        rejet.setBlocking(ex.getTypeRejet().isBlocking());
        return rejet;
    }
}
