package nc.unc.importparcoursup.utils;

public class SpecialCharUtils {
    public static final String CHECKED = "🗸";
    public static final String CROSSED = "✗";
    public static final String CIRCLE_ONE = "①";
    public static final String CIRCLE_TWO = "②";
    public static final String CIRCLE_THREE = "③";
}
