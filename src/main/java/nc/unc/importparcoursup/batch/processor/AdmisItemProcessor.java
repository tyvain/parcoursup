package nc.unc.importparcoursup.batch.processor;

import nc.unc.importparcoursup.batch.mail.EmailService;
import nc.unc.importparcoursup.batch.mail.Mail;
import nc.unc.importparcoursup.batch.mappers.AdmisToPreCandidatMapper;
import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidature;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationSpecialisation;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.*;
import nc.unc.importparcoursup.model.AdmisSkipException;
import nc.unc.importparcoursup.model.TypeRejet;
import nc.unc.importparcoursup.utils.MapperUtils;
import nc.unc.importparcoursup.utils.ScolUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

public class AdmisItemProcessor implements ItemProcessor<Admis, PreCandidat> {
    private static final Logger log = LoggerFactory.getLogger(AdmisItemProcessor.class);
    private PreCandidatRepository preCandidatRepository;
    private ScolFormationSpecialisationRepository scolFormationSpecialisationRepository;
    private BacRepository bacRepository;
    private AdmisRejetRepository admisRejetRepository;
    private EmailService emailService;
    private HashSet<String> codePaysSet = new HashSet<>();

    public AdmisItemProcessor(PreCandidatRepository preCandidatRepository,
                              ScolFormationSpecialisationRepository scolFormationSpecialisationRepository,
                              BacRepository bacRepository,
                              PaysRepository paysRepository,
                              AdmisRejetRepository admisRejetRepository,
                              EmailService emailService) {
        this.preCandidatRepository = preCandidatRepository;
        this.scolFormationSpecialisationRepository = scolFormationSpecialisationRepository;
        this.preCandidatRepository = preCandidatRepository;
        this.bacRepository = bacRepository;
        this.admisRejetRepository = admisRejetRepository;
        this.emailService = emailService;
        paysRepository.findAll().forEach(pays -> codePaysSet.add(pays.getCodepays()));
    }

    @Override
    public PreCandidat process(Admis admis) throws AdmisSkipException {
        checkBacCode(admis);
        checkCodesPays(admis, admis.getPaysCodeDerEtab(), admis.getPaysCodeNais(), admis.getPaysEtabBac(), admis.getCandPaysParent());
        ScolFormationSpecialisation scolFormationSpecialisation = getScolFormationSpecialisation(admis);
        // récupération du candidat
        Optional<PreCandidat> preCandidatOpt = preCandidatRepository.findById(Long.parseLong(admis.getCandNumero()));
        PreCandidat ret = preCandidatOpt
                .map(preCandidat -> updatePreCandidat(preCandidat, admis, scolFormationSpecialisation))
                .orElseGet(() -> createPreCandidat(admis, scolFormationSpecialisation));
        return ret;
    }

    private void checkBacCode(Admis admis) throws AdmisSkipException {
        if (!bacRepository.existsByBaccode(admis.getBacCode())) {
            throw new AdmisSkipException(TypeRejet.CODE_BAC_INCONNU, admis, admis.getBacCode());
        }
    }

    // si le code pays est inconnu, on sauve dans les rejets et on créé le pré candidat (
    private void checkCodesPays(Admis admis, String... codePays) {
        Arrays.stream(codePays)
                .filter(code -> StringUtils.isNotBlank(code) && !code.equalsIgnoreCase("null"))
                .forEach(code -> {
                    // if (!paysRepository.existsByCodepays(ScolUtils.getPays(code))) {
                    if (!codePaysSet.contains(ScolUtils.getPays(code))) {
                        log.error("CODE_PAYS_INCONNU: {} {} {}", codePaysSet.size(), admis.getCandNumero(), code);
                        admisRejetRepository.save(MapperUtils.getAdmisRejetFromAdmisSkipException(new AdmisSkipException(TypeRejet.CODE_PAYS_INCONNU, admis, code)));
                    }
                });
    }

    private PreCandidat updatePreCandidat(PreCandidat preCandidat, Admis admis, ScolFormationSpecialisation scolFormationSpecialisation) {
        log.debug("-- MAJ d'un préCandidat existant");
        if (isSameFormation(scolFormationSpecialisation, preCandidat)) {
            return buildPreCandidat(admis, preCandidat, preCandidat.getPreCandidature());
        } else {
            log.debug("---- le candidat a changé de formation");
            //TODO que faire dans ce cas ?
            return buildPreCandidat(admis, preCandidat, preCandidat.getPreCandidature());
        }
    }

    private boolean isSameFormation(ScolFormationSpecialisation scolFormationSpecialisation, PreCandidat preCandidat) {
        return scolFormationSpecialisation.getFspnkey().equals(preCandidat.getPreCandidature().getScolFormationSpecialisation().getFspnkey());
    }

    private PreCandidat createPreCandidat(Admis admis, ScolFormationSpecialisation scolFormationSpecialisation) {
        log.debug("-- Création d'un nouveau préCandidat");
        PreCandidature preCandidature = new PreCandidature();
        preCandidature.setCANU_KEY(MapperUtils.stringToLong(admis.getCandNumero()));
        preCandidature.setScolFormationSpecialisation(scolFormationSpecialisation);
        preCandidature.setCAND_ANNEE_SUIVIE(1);
        PreCandidat preCandidat = buildPreCandidat(admis, new PreCandidat(), preCandidature);

        log.info("Spring Mail - Sending Simple Email with JavaMailSender Example");

        Mail mail = new Mail();
        mail.setFrom("univ@univ.nc");
        mail.setTo(admis.getCandEmailScol());
        mail.setSubject("Vous pouvez désormais vous pré-inscrire à l'UNC");
        mail.setContent("Bonjour,\n Vous pouvez désormais vous connecter au portail de préinscription à l'UNC.");

        emailService.sendSimpleMessage(mail);

        return preCandidat;
    }

    private PreCandidat buildPreCandidat(Admis admis, PreCandidat preCandidat, PreCandidature preCandidature) {
        AdmisToPreCandidatMapper.convertAdmisIntoPreCandidat(admis, preCandidat);
        preCandidat.setPreCandidature(preCandidature);
        preCandidature.setPreCandidat(preCandidat);
        log.debug("---- -admis.getCandNumero(): {} -preCandidat.getCAND_NUMERO: {} -preCandidature.CANU_KEY: {}", admis.getCandNumero(), preCandidat.getCAND_NUMERO(), preCandidature.getCANU_KEY());
        return preCandidat;
    }

    private ScolFormationSpecialisation getScolFormationSpecialisation(Admis admis) throws AdmisSkipException {
        Optional<ScolFormationSpecialisation> scolFormationSpecialisation;
        try {
            scolFormationSpecialisation = scolFormationSpecialisationRepository.findByFdipCode(admis.getFDipCode(), ScolUtils.getCurrentScolYear());
        } catch (IncorrectResultSizeDataAccessException e) {
            //log.error("CODE_DIPLOME_EN_DOUBLE: -admis.getCandNumero: {} -admis.getFDipCode: {} -ScolUtils.getCurrentScolYear(): {}", admis.getCandNumero(), admis.getFDipCode(), ScolUtils.getCurrentScolYear());
            throw new AdmisSkipException(TypeRejet.CODE_DIPLOME_EN_DOUBLE, admis, admis.getFDipCode());
        }
        return scolFormationSpecialisation.orElseThrow(() -> new AdmisSkipException(TypeRejet.CODE_DIPLOME_INCONNU, admis, admis.getFDipCode()));
    }
}