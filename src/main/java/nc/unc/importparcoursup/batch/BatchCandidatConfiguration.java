package nc.unc.importparcoursup.batch;

import nc.unc.importparcoursup.batch.io.candidat.CandidatFileReader;
import nc.unc.importparcoursup.batch.io.candidat.CandidatRepoWriter;
import nc.unc.importparcoursup.dao.candidatDAO.Candidat;
import nc.unc.importparcoursup.dao.candidatDAO.CandidatHistory;
import nc.unc.importparcoursup.dao.candidatDAO.repository.CandidatHistoryRepository;
import nc.unc.importparcoursup.dao.candidatDAO.repository.CandidatRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.IOException;

@Configuration
@EnableBatchProcessing
public class BatchCandidatConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private CandidatRepository CandidatRepository;

    @Autowired
    private CandidatHistoryRepository CandidatHistoryRepository;

    @Value("${file.local-tmp-file}")
    private String inputFile;

    @Value("${file.archive-directory}")
    private FileSystemResource archiveDirectory;

    @Bean
    @StepScope
    public CandidatFileReader readerFileCandidat(@Value("#{jobParameters[fileName]}") String fileName,
                                                 @Value("#{jobParameters[userLogin]}") String userLogin,
                                                 @Value("#{jobParameters[fileCheckSum]}") String fileCheckSum,
                                                 @Value("#{jobParameters[uploadedFileName]}") String uploadedFileName) throws IOException {
        CandidatHistory CandidatHistory = new CandidatHistory(userLogin, uploadedFileName, fileCheckSum);
        CandidatHistoryRepository.save(CandidatHistory);
        return new CandidatFileReader(new File(fileName), CandidatHistory);
    }

    @Bean
    @StepScope
    public CandidatRepoWriter writerDBCandidat() {
        return new CandidatRepoWriter(CandidatRepository);
    }

    @Bean
    @Qualifier("importCandidatJob")
    public Job importCandidatJob(Step step1_readCandidatFileWriteDB) {
        return jobBuilderFactory.get("importCandidatJob")
                .incrementer(new RunIdIncrementer())
                .start(step1_readCandidatFileWriteDB)
                .build();
    }

    @Bean
    public Step step1_readCandidatFileWriteDB(CandidatRepository repository) throws Exception {
        return stepBuilderFactory.get("step1_readCandidatFileWriteDB")
                .<Candidat, Candidat>chunk(1000)
                .reader(readerFileCandidat("noParamYet", "noParamYet", "noParamYet", "noParamYet"))
                .writer(writerDBCandidat())
                .build();
    }
}
