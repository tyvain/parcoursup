package nc.unc.importparcoursup.dao.candidatDAO;

import nc.unc.importparcoursup.dao.IHistory;
import nc.unc.importparcoursup.model.UAI;
import nc.unc.importparcoursup.utils.MyFileUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = CandidatHistory.TABLE_NAME)
public class CandidatHistory implements IHistory {
    public final static String TABLE_NAME = "CANDIDAT_HISTORY";

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private Date jobExecutionDate;

    private String fileCheckSum;

    @Enumerated(EnumType.STRING)
    private UAI uai;

    private Date extractionDate;

    private String createdBy;

    private String fileName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "candidatHistory", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Candidat> candidats = new ArrayList<>();

    public CandidatHistory() {
        // Needed for hibernate
    }

    public CandidatHistory(String userLogin, String fileName, String fileCheckSum) {
        this.jobExecutionDate = new Date();
        this.extractionDate = MyFileUtils.getDateFromFileName(fileName);
        this.uai = MyFileUtils.getUAIFromFileName(fileName);
        this.fileCheckSum = fileCheckSum;
        this.createdBy = userLogin;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getJobExecutionDate() {
        return jobExecutionDate;
    }

    public void setJobExecutionDate(Date jobExecutionDate) {
        this.jobExecutionDate = jobExecutionDate;
    }

    public List<Candidat> getCandidat() {
        return candidats;
    }

    public void setCandidat(List<Candidat> candidats) {
        this.candidats = candidats;
    }

    public String getFileCheckSum() {
        return fileCheckSum;
    }

    public void setFileCheckSum(String fileCheckSum) {
        this.fileCheckSum = fileCheckSum;
    }

    public UAI getUai() {
        return uai;
    }

    public void setUai(UAI uai) {
        this.uai = uai;
    }

    public Date getExtractionDate() {
        return extractionDate;
    }

    public void setExtractionDate(Date extractionDate) {
        this.extractionDate = extractionDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<Candidat> getCandidats() {
        return candidats;
    }

    public void setCandidats(List<Candidat> candidats) {
        this.candidats = candidats;
    }
}
