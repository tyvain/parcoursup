package nc.unc.importparcoursup.dao.candidatDAO;

import com.opencsv.bean.CsvBindAndJoinByName;
import com.opencsv.bean.CsvBindByName;

import javax.persistence.*;

@SuppressWarnings("NonAsciiCharacters")
@Entity
@Table(name = Candidat.TABLE_NAME)
public class Candidat {
    public final static String TABLE_NAME = "CANDIDAT";
    public final static String CANDIDAT_HISTORY_ID = "CANDIDAT_HISTORY_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = CANDIDAT_HISTORY_ID)
    private CandidatHistory candidatHistory;

    // Obligé de supprimé ce qui précéde la premiere colonne
    // car le fichier en encodé en UTF-8 BOM (qui rajoute des caractere au début de ligne)
    @CsvBindByName(column = "\uFEFFGroupe")
    private String groupe;
    @CsvBindByName(column = "Classement")
    private String classement;
    @CsvBindByName(column = "Code aménagement (oui - si)")
    private String codeAmenagementOuiSi;
    @CsvBindByName(column = "Numéro", required = true)
    private String numero;
    @CsvBindByName(column = "Code formation")
    private String codeFormation;
    @CsvBindByName(column = " Formation")
    private String formation;
    @CsvBindByName(column = " Filière")
    private String filiere;
    @CsvBindByName(column = "Nom")
    private String nom;
    @CsvBindByName(column = "Prénom")
    private String prenom;
    @CsvBindByName(column = "Libelle groupe")
    private String libelleGroupe;
    @CsvBindByName(column = "Deuxième prénom")
    private String deuxiemePrenom;
    @CsvBindByName(column = "Troisième prénom")
    private String troisiemePrenom;
    @CsvBindByName(column = "Civilité")
    private String civilite;
    @CsvBindByName(column = "Sexe")
    private String sexe;
    @CsvBindByName(column = "Date de naissance")
    private String dateDeNaissance;
    @CsvBindByName(column = "Ville de naissance (Code)")
    private String villeDeNaissanceCode;
    @CsvBindByName(column = "Ville de naissance")
    private String villeDeNaissance;
    @CsvBindByName(column = "Département de naissance")
    private String departementDeNaissance;
    @CsvBindByName(column = "Pays de naissance (Code)")
    private String paysDeNaissanceCode;
    @CsvBindByName(column = "Pays de naissance")
    private String paysDeNaissance;
    @CsvBindByName(column = "Nationalité")
    private String nationalite;
    @CsvBindByName(column = "Boursier des lycées")
    private String boursierDesLycees;
    @CsvBindByName(column = "Boursier des lycées (code)")
    private String boursierDesLyceesCode;
    @CsvBindByName(column = "Boursier (Code)")
    private String boursierCode;
    @CsvBindByName(column = "Boursier")
    private String boursier;
    @CsvBindByName(column = "Boursier certifié")
    private String boursierCertifie;
    @CsvBindByName(column = "Boursier certifié (Code)")
    private String boursierCertifieCode;
    @CsvBindByName(column = "Nombre de parts ou échelon")
    private String nombreDePartsOuEchelon;
    @CsvBindByName(column = "ACB (Code)")
    private String aCBCode;
    @CsvBindByName(column = "ACB")
    private String aCB;
    @CsvBindByName(column = "Profil du candidat")
    private String profilDuCandidat;
    @CsvBindByName(column = "Profil du candidat (Code)")
    private String profilDuCandidatCode;
    @CsvBindByName(column = "Type établissement")
    private String typeEtablissement;
    @CsvBindByName(column = "Type établissement (Code)")
    private String typeEtablissementCode;
    @CsvBindByName(column = "Contrat établissement")
    private String contratEtablissement;
    @CsvBindByName(column = "Contrat établissement (Code)")
    private String contratEtablissementCode;
    @CsvBindByName(column = "UAI établissement")
    private String uAIEtablissement;
    @CsvBindByName(column = "Libellé établissement")
    private String libelleEtablissement;
    @CsvBindByName(column = "Commune établissement")
    private String communeEtablissement;
    @CsvBindByName(column = "Département établissement")
    private String departementEtablissement;
    @CsvBindByName(column = "Pays établissement")
    private String paysEtablissement;
    @CsvBindByName(column = "Pays établissement (Code)")
    private String paysEtablissementCode;
    @CsvBindByName(column = "Téléphone établissement")
    private String telephoneEtablissement;
    @CsvBindByName(column = "Fax établissement")
    private String faxEtablissement;
    @CsvBindByName(column = "Niveau d'étude actuelle")
    private String niveauDEtudeActuelle;
    @CsvBindByName(column = "Niveau d'étude actuelle")
    private String niveauDEtudeActuelleCode;
    @CsvBindByName(column = "Numéro INE")
    private String numeroINE;
    @CsvBindByName(column = "Numéro CEF")
    private String numeroCEF;
    @CsvBindByName(column = "Type de formation (Code)")
    private String typeDeFormationCode;
    @CsvBindByName(column = "Type de formation")
    private String typeDeFormation;
    @CsvBindByName(column = "Série/Domaine/Filière")
    private String serieDomaineFiliere;
    @CsvBindByName(column = "Classe")
    private String classe;
    @CsvBindByName(column = "Option internationale (Code)")
    private String optionInternationaleCode;
    @CsvBindByName(column = "Option internationale")
    private String optionInternationale;
    @CsvBindByName(column = "Dominante")
    private String dominante;
    @CsvBindByName(column = "Dominante (Code)")
    private String dominanteCode;
    @CsvBindByName(column = "Spécialité/Mention/Voie")
    private String specialiteMentionVoie;
    @CsvBindByName(column = "Spécialité (Code)")
    private String specialiteCode;
    @CsvBindByName(column = "LV 1 scolarité")
    private String lV1Scolarite;
    @CsvBindByName(column = "LV 1 (Code)")
    private String lV1Code;
    @CsvBindByName(column = "LV 2 scolarité")
    private String lV2Scolarite;
    @CsvBindByName(column = "LV 2 (Code)")
    private String lV2Code;
    @CsvBindByName(column = "LV 3 scolarité")
    private String lV3Scolarite;
    @CsvBindByName(column = "LV 3 (Code)")
    private String lV3Code;
    @CsvBindByName(column = "Option 1")
    private String option1;
    @CsvBindByName(column = "Option 1 (Code)")
    private String option1Code;
    @CsvBindByName(column = "Option 2")
    private String option2;
    @CsvBindByName(column = "Option 2 (Code)")
    private String option2Code;
    @CsvBindByName(column = "Diplôme")
    private String diplome;
    @CsvBindByName(column = "Diplôme (Code)")
    private String diplomeCode;
    @CsvBindByName(column = "Série diplôme (Code)")
    private String serieDiplomeCode;
    @CsvBindByName(column = "Série diplôme")
    private String serieDiplome;
    @CsvBindByName(column = "Dominante diplôme")
    private String dominanteDiplome;
    @CsvBindByName(column = "Dominante diplôme (Code)")
    private String dominanteDiplomeCode;
    @CsvBindByName(column = "Spécialité diplôme")
    private String specialiteDiplome;
    @CsvBindByName(column = "Spécialité diplôme (Code)")
    private String specialiteDiplomeCode;
    @CsvBindByName(column = "Mention diplôme")
    private String mentionDiplome;
    @CsvBindByName(column = "Mention diplôme (Code)")
    private String mentionDiplomeCode;
    @CsvBindByName(column = "Mois et année d'obtention du bac")
    private String moisEtAnneeDObtentionDuBac;
    @CsvBindByName(column = "Académie du bac")
    private String academieDuBac;
    @CsvBindByName(column = "Académie du bac (Code)")
    private String academieDuBacCode;
    @CsvBindByName(column = "LV 1 Bac")
    private String lV1Bac;
    @CsvBindByName(column = "LV1 Bac (Code)")
    private String lV1BacCode;
    @CsvBindByName(column = "LV 2 Bac")
    private String lV2Bac;
    @CsvBindByName(column = "LV2 Bac (Code)")
    private String lV2BacCode;
    @CsvBindByName(column = "Option du bac (Code)")
    private String optionDuBacCode;
    @CsvBindByName(column = "Option du bac")
    private String optionDuBac;
    @CsvBindByName(column = "Diplôme étranger (Code)")
    private String diplomeEtrangerCode;
    @CsvBindByName(column = "Diplôme étranger")
    private String diplomeEtranger;
    @CsvBindByName(column = "Test de français")
    private String testDeFrançais;
    @CsvBindByName(column = "Test de français (Code)")
    private String testDeFrançaisCode;
    @CsvBindByName(column = "Niveau de français")
    private String niveauDeFrançais;
    @CsvBindByName(column = "Date test de français")
    private String dateTestDeFrançais;
    @CsvBindByName(column = "Aménagement handicap (code)")
    private String amenagementHandicapCode;
    @CsvBindByName(column = "Aménagement handicap")
    private String amenagementHandicap;
    @CsvBindByName(column = "Date de validation")
    private String dateDeValidation;
    @CsvBindByName(column = "Candidature validée (Code)")
    private String candidatureValideeCode;
    @CsvBindByName(column = "Candidature validée (O/N)")
    private String candidatureValideeON;
    @CsvBindByName(column = "Sportif de haut niveau (code)")
    private String sportifDeHautNiveauCode;
    @CsvBindByName(column = "Sportif de haut niveau")
    private String sportifDeHautNiveau;
    @CsvBindByName(column = "Artiste de haut niveau")
    private String artisteDeHautNiveau;
    @CsvBindByName(column = "Artiste de haut niveau (code)")
    private String artisteDeHautNiveauCode;
    @CsvBindByName(column = "Date inscription")
    private String dateInscription;
    @CsvBindByName(column = "Date d'impression")
    private String dateDImpression;
    @CsvBindByName(column = "Fiche imprimée (O/N)")
    private String ficheImprimeeON;
    @CsvBindByName(column = "Fiche imprimée (Code)")
    private String ficheImprimeeCode;
    @CsvBindByName(column = "Eligible à la politique de la ville")
    private String eligibleàLaPolitiqueDeLaVille;
    @CsvBindByName(column = "Fiche de réorientation")
    private String ficheDeReorientation;
    @CsvBindByName(column = "Fiche de réorientation (code)")
    private String ficheDeReorientationCode;
    @CsvBindByName(column = "Dossier papier")
    private String dossierPapier;
    @CsvBindByName(column = "Dossier papier (code)")
    private String dossierPapierCode;
    @CsvBindByName(column = "Année d'entrée en seconde")
    private String anneeDEntreeEnSeconde;
    @CsvBindByName(column = "Dossier dématérialisé (code)")
    private String dossierDematerialiseCode;
    @CsvBindByName(column = "Dossier dématérialisé")
    private String dossierDematerialise;

    public String getGroupe() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getClassement() {
        return classement;
    }

    public void setClassement(String classement) {
        this.classement = classement;
    }

    public String getCodeAmenagementOuiSi() {
        return codeAmenagementOuiSi;
    }

    public void setCodeAmenagementOuiSi(String codeAmenagementOuiSi) {
        this.codeAmenagementOuiSi = codeAmenagementOuiSi;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodeFormation() {
        return codeFormation;
    }

    public void setCodeFormation(String codeFormation) {
        this.codeFormation = codeFormation;
    }

    public String getFormation() {
        return formation;
    }

    public void setFormation(String formation) {
        this.formation = formation;
    }

    public String getFiliere() {
        return filiere;
    }

    public void setFiliere(String filiere) {
        this.filiere = filiere;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLibelleGroupe() {
        return libelleGroupe;
    }

    public void setLibelleGroupe(String libelleGroupe) {
        this.libelleGroupe = libelleGroupe;
    }

    public String getDeuxiemePrenom() {
        return deuxiemePrenom;
    }

    public void setDeuxiemePrenom(String deuxiemePrenom) {
        this.deuxiemePrenom = deuxiemePrenom;
    }

    public String getTroisiemePrenom() {
        return troisiemePrenom;
    }

    public void setTroisiemePrenom(String troisiemePrenom) {
        this.troisiemePrenom = troisiemePrenom;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(String dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getVilleDeNaissanceCode() {
        return villeDeNaissanceCode;
    }

    public void setVilleDeNaissanceCode(String villeDeNaissanceCode) {
        this.villeDeNaissanceCode = villeDeNaissanceCode;
    }

    public String getVilleDeNaissance() {
        return villeDeNaissance;
    }

    public void setVilleDeNaissance(String villeDeNaissance) {
        this.villeDeNaissance = villeDeNaissance;
    }

    public String getDepartementDeNaissance() {
        return departementDeNaissance;
    }

    public void setDepartementDeNaissance(String departementDeNaissance) {
        this.departementDeNaissance = departementDeNaissance;
    }

    public String getPaysDeNaissanceCode() {
        return paysDeNaissanceCode;
    }

    public void setPaysDeNaissanceCode(String paysDeNaissanceCode) {
        this.paysDeNaissanceCode = paysDeNaissanceCode;
    }

    public String getPaysDeNaissance() {
        return paysDeNaissance;
    }

    public void setPaysDeNaissance(String paysDeNaissance) {
        this.paysDeNaissance = paysDeNaissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getBoursierDesLycees() {
        return boursierDesLycees;
    }

    public void setBoursierDesLycees(String boursierDesLycees) {
        this.boursierDesLycees = boursierDesLycees;
    }

    public String getBoursierDesLyceesCode() {
        return boursierDesLyceesCode;
    }

    public void setBoursierDesLyceesCode(String boursierDesLyceesCode) {
        this.boursierDesLyceesCode = boursierDesLyceesCode;
    }

    public String getBoursierCode() {
        return boursierCode;
    }

    public void setBoursierCode(String boursierCode) {
        this.boursierCode = boursierCode;
    }

    public String getBoursier() {
        return boursier;
    }

    public void setBoursier(String boursier) {
        this.boursier = boursier;
    }

    public String getBoursierCertifie() {
        return boursierCertifie;
    }

    public void setBoursierCertifie(String boursierCertifie) {
        this.boursierCertifie = boursierCertifie;
    }

    public String getBoursierCertifieCode() {
        return boursierCertifieCode;
    }

    public void setBoursierCertifieCode(String boursierCertifieCode) {
        this.boursierCertifieCode = boursierCertifieCode;
    }

    public String getNombreDePartsOuEchelon() {
        return nombreDePartsOuEchelon;
    }

    public void setNombreDePartsOuEchelon(String nombreDePartsOuEchelon) {
        this.nombreDePartsOuEchelon = nombreDePartsOuEchelon;
    }

    public String getaCBCode() {
        return aCBCode;
    }

    public void setaCBCode(String aCBCode) {
        this.aCBCode = aCBCode;
    }

    public String getaCB() {
        return aCB;
    }

    public void setaCB(String aCB) {
        this.aCB = aCB;
    }

    public String getProfilDuCandidat() {
        return profilDuCandidat;
    }

    public void setProfilDuCandidat(String profilDuCandidat) {
        this.profilDuCandidat = profilDuCandidat;
    }

    public String getProfilDuCandidatCode() {
        return profilDuCandidatCode;
    }

    public void setProfilDuCandidatCode(String profilDuCandidatCode) {
        this.profilDuCandidatCode = profilDuCandidatCode;
    }

    public String getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(String typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public String getTypeEtablissementCode() {
        return typeEtablissementCode;
    }

    public void setTypeEtablissementCode(String typeEtablissementCode) {
        this.typeEtablissementCode = typeEtablissementCode;
    }

    public String getContratEtablissement() {
        return contratEtablissement;
    }

    public void setContratEtablissement(String contratEtablissement) {
        this.contratEtablissement = contratEtablissement;
    }

    public String getContratEtablissementCode() {
        return contratEtablissementCode;
    }

    public void setContratEtablissementCode(String contratEtablissementCode) {
        this.contratEtablissementCode = contratEtablissementCode;
    }

    public String getuAIEtablissement() {
        return uAIEtablissement;
    }

    public void setuAIEtablissement(String uAIEtablissement) {
        this.uAIEtablissement = uAIEtablissement;
    }

    public String getLibelleEtablissement() {
        return libelleEtablissement;
    }

    public void setLibelleEtablissement(String libelleEtablissement) {
        this.libelleEtablissement = libelleEtablissement;
    }

    public String getCommuneEtablissement() {
        return communeEtablissement;
    }

    public void setCommuneEtablissement(String communeEtablissement) {
        this.communeEtablissement = communeEtablissement;
    }

    public String getDepartementEtablissement() {
        return departementEtablissement;
    }

    public void setDepartementEtablissement(String departementEtablissement) {
        this.departementEtablissement = departementEtablissement;
    }

    public String getPaysEtablissement() {
        return paysEtablissement;
    }

    public void setPaysEtablissement(String paysEtablissement) {
        this.paysEtablissement = paysEtablissement;
    }

    public String getPaysEtablissementCode() {
        return paysEtablissementCode;
    }

    public void setPaysEtablissementCode(String paysEtablissementCode) {
        this.paysEtablissementCode = paysEtablissementCode;
    }

    public String getTelephoneEtablissement() {
        return telephoneEtablissement;
    }

    public void setTelephoneEtablissement(String telephoneEtablissement) {
        this.telephoneEtablissement = telephoneEtablissement;
    }

    public String getFaxEtablissement() {
        return faxEtablissement;
    }

    public void setFaxEtablissement(String faxEtablissement) {
        this.faxEtablissement = faxEtablissement;
    }

    public String getNiveauDEtudeActuelle() {
        return niveauDEtudeActuelle;
    }

    public void setNiveauDEtudeActuelle(String niveauDEtudeActuelle) {
        this.niveauDEtudeActuelle = niveauDEtudeActuelle;
    }

    public String getNiveauDEtudeActuelleCode() {
        return niveauDEtudeActuelleCode;
    }

    public void setNiveauDEtudeActuelleCode(String niveauDEtudeActuelleCode) {
        this.niveauDEtudeActuelleCode = niveauDEtudeActuelleCode;
    }

    public String getNumeroINE() {
        return numeroINE;
    }

    public void setNumeroINE(String numeroINE) {
        this.numeroINE = numeroINE;
    }

    public String getNumeroCEF() {
        return numeroCEF;
    }

    public void setNumeroCEF(String numeroCEF) {
        this.numeroCEF = numeroCEF;
    }

    public String getTypeDeFormationCode() {
        return typeDeFormationCode;
    }

    public void setTypeDeFormationCode(String typeDeFormationCode) {
        this.typeDeFormationCode = typeDeFormationCode;
    }

    public String getTypeDeFormation() {
        return typeDeFormation;
    }

    public void setTypeDeFormation(String typeDeFormation) {
        this.typeDeFormation = typeDeFormation;
    }

    public String getSerieDomaineFiliere() {
        return serieDomaineFiliere;
    }

    public void setSerieDomaineFiliere(String serieDomaineFiliere) {
        this.serieDomaineFiliere = serieDomaineFiliere;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getOptionInternationaleCode() {
        return optionInternationaleCode;
    }

    public void setOptionInternationaleCode(String optionInternationaleCode) {
        this.optionInternationaleCode = optionInternationaleCode;
    }

    public String getOptionInternationale() {
        return optionInternationale;
    }

    public void setOptionInternationale(String optionInternationale) {
        this.optionInternationale = optionInternationale;
    }

    public String getDominante() {
        return dominante;
    }

    public void setDominante(String dominante) {
        this.dominante = dominante;
    }

    public String getDominanteCode() {
        return dominanteCode;
    }

    public void setDominanteCode(String dominanteCode) {
        this.dominanteCode = dominanteCode;
    }

    public String getSpecialiteMentionVoie() {
        return specialiteMentionVoie;
    }

    public void setSpecialiteMentionVoie(String specialiteMentionVoie) {
        this.specialiteMentionVoie = specialiteMentionVoie;
    }

    public String getSpecialiteCode() {
        return specialiteCode;
    }

    public void setSpecialiteCode(String specialiteCode) {
        this.specialiteCode = specialiteCode;
    }

    public String getlV1Scolarite() {
        return lV1Scolarite;
    }

    public void setlV1Scolarite(String lV1Scolarite) {
        this.lV1Scolarite = lV1Scolarite;
    }

    public String getlV1Code() {
        return lV1Code;
    }

    public void setlV1Code(String lV1Code) {
        this.lV1Code = lV1Code;
    }

    public String getlV2Scolarite() {
        return lV2Scolarite;
    }

    public void setlV2Scolarite(String lV2Scolarite) {
        this.lV2Scolarite = lV2Scolarite;
    }

    public String getlV2Code() {
        return lV2Code;
    }

    public void setlV2Code(String lV2Code) {
        this.lV2Code = lV2Code;
    }

    public String getlV3Scolarite() {
        return lV3Scolarite;
    }

    public void setlV3Scolarite(String lV3Scolarite) {
        this.lV3Scolarite = lV3Scolarite;
    }

    public String getlV3Code() {
        return lV3Code;
    }

    public void setlV3Code(String lV3Code) {
        this.lV3Code = lV3Code;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption1Code() {
        return option1Code;
    }

    public void setOption1Code(String option1Code) {
        this.option1Code = option1Code;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption2Code() {
        return option2Code;
    }

    public void setOption2Code(String option2Code) {
        this.option2Code = option2Code;
    }

    public String getDiplome() {
        return diplome;
    }

    public void setDiplome(String diplome) {
        this.diplome = diplome;
    }

    public String getDiplomeCode() {
        return diplomeCode;
    }

    public void setDiplomeCode(String diplomeCode) {
        this.diplomeCode = diplomeCode;
    }

    public String getSerieDiplomeCode() {
        return serieDiplomeCode;
    }

    public void setSerieDiplomeCode(String serieDiplomeCode) {
        this.serieDiplomeCode = serieDiplomeCode;
    }

    public String getSerieDiplome() {
        return serieDiplome;
    }

    public void setSerieDiplome(String serieDiplome) {
        this.serieDiplome = serieDiplome;
    }

    public String getDominanteDiplome() {
        return dominanteDiplome;
    }

    public void setDominanteDiplome(String dominanteDiplome) {
        this.dominanteDiplome = dominanteDiplome;
    }

    public String getDominanteDiplomeCode() {
        return dominanteDiplomeCode;
    }

    public void setDominanteDiplomeCode(String dominanteDiplomeCode) {
        this.dominanteDiplomeCode = dominanteDiplomeCode;
    }

    public String getSpecialiteDiplome() {
        return specialiteDiplome;
    }

    public void setSpecialiteDiplome(String specialiteDiplome) {
        this.specialiteDiplome = specialiteDiplome;
    }

    public String getSpecialiteDiplomeCode() {
        return specialiteDiplomeCode;
    }

    public void setSpecialiteDiplomeCode(String specialiteDiplomeCode) {
        this.specialiteDiplomeCode = specialiteDiplomeCode;
    }

    public String getMentionDiplome() {
        return mentionDiplome;
    }

    public void setMentionDiplome(String mentionDiplome) {
        this.mentionDiplome = mentionDiplome;
    }

    public String getMentionDiplomeCode() {
        return mentionDiplomeCode;
    }

    public void setMentionDiplomeCode(String mentionDiplomeCode) {
        this.mentionDiplomeCode = mentionDiplomeCode;
    }

    public String getMoisEtAnneeDObtentionDuBac() {
        return moisEtAnneeDObtentionDuBac;
    }

    public void setMoisEtAnneeDObtentionDuBac(String moisEtAnneeDObtentionDuBac) {
        this.moisEtAnneeDObtentionDuBac = moisEtAnneeDObtentionDuBac;
    }

    public String getAcademieDuBac() {
        return academieDuBac;
    }

    public void setAcademieDuBac(String academieDuBac) {
        this.academieDuBac = academieDuBac;
    }

    public String getAcademieDuBacCode() {
        return academieDuBacCode;
    }

    public void setAcademieDuBacCode(String academieDuBacCode) {
        this.academieDuBacCode = academieDuBacCode;
    }

    public String getlV1Bac() {
        return lV1Bac;
    }

    public void setlV1Bac(String lV1Bac) {
        this.lV1Bac = lV1Bac;
    }

    public String getlV1BacCode() {
        return lV1BacCode;
    }

    public void setlV1BacCode(String lV1BacCode) {
        this.lV1BacCode = lV1BacCode;
    }

    public String getlV2Bac() {
        return lV2Bac;
    }

    public void setlV2Bac(String lV2Bac) {
        this.lV2Bac = lV2Bac;
    }

    public String getlV2BacCode() {
        return lV2BacCode;
    }

    public void setlV2BacCode(String lV2BacCode) {
        this.lV2BacCode = lV2BacCode;
    }

    public String getOptionDuBacCode() {
        return optionDuBacCode;
    }

    public void setOptionDuBacCode(String optionDuBacCode) {
        this.optionDuBacCode = optionDuBacCode;
    }

    public String getOptionDuBac() {
        return optionDuBac;
    }

    public void setOptionDuBac(String optionDuBac) {
        this.optionDuBac = optionDuBac;
    }

    public String getDiplomeEtrangerCode() {
        return diplomeEtrangerCode;
    }

    public void setDiplomeEtrangerCode(String diplomeEtrangerCode) {
        this.diplomeEtrangerCode = diplomeEtrangerCode;
    }

    public String getDiplomeEtranger() {
        return diplomeEtranger;
    }

    public void setDiplomeEtranger(String diplomeEtranger) {
        this.diplomeEtranger = diplomeEtranger;
    }

    public String getTestDeFrançais() {
        return testDeFrançais;
    }

    public void setTestDeFrançais(String testDeFrançais) {
        this.testDeFrançais = testDeFrançais;
    }

    public String getTestDeFrançaisCode() {
        return testDeFrançaisCode;
    }

    public void setTestDeFrançaisCode(String testDeFrançaisCode) {
        this.testDeFrançaisCode = testDeFrançaisCode;
    }

    public String getNiveauDeFrançais() {
        return niveauDeFrançais;
    }

    public void setNiveauDeFrançais(String niveauDeFrançais) {
        this.niveauDeFrançais = niveauDeFrançais;
    }

    public String getDateTestDeFrançais() {
        return dateTestDeFrançais;
    }

    public void setDateTestDeFrançais(String dateTestDeFrançais) {
        this.dateTestDeFrançais = dateTestDeFrançais;
    }

    public String getAmenagementHandicapCode() {
        return amenagementHandicapCode;
    }

    public void setAmenagementHandicapCode(String amenagementHandicapCode) {
        this.amenagementHandicapCode = amenagementHandicapCode;
    }

    public String getAmenagementHandicap() {
        return amenagementHandicap;
    }

    public void setAmenagementHandicap(String amenagementHandicap) {
        this.amenagementHandicap = amenagementHandicap;
    }

    public String getDateDeValidation() {
        return dateDeValidation;
    }

    public void setDateDeValidation(String dateDeValidation) {
        this.dateDeValidation = dateDeValidation;
    }

    public String getCandidatureValideeCode() {
        return candidatureValideeCode;
    }

    public void setCandidatureValideeCode(String candidatureValideeCode) {
        this.candidatureValideeCode = candidatureValideeCode;
    }

    public String getCandidatureValideeON() {
        return candidatureValideeON;
    }

    public void setCandidatureValideeON(String candidatureValideeON) {
        this.candidatureValideeON = candidatureValideeON;
    }

    public String getSportifDeHautNiveauCode() {
        return sportifDeHautNiveauCode;
    }

    public void setSportifDeHautNiveauCode(String sportifDeHautNiveauCode) {
        this.sportifDeHautNiveauCode = sportifDeHautNiveauCode;
    }

    public String getSportifDeHautNiveau() {
        return sportifDeHautNiveau;
    }

    public void setSportifDeHautNiveau(String sportifDeHautNiveau) {
        this.sportifDeHautNiveau = sportifDeHautNiveau;
    }

    public String getArtisteDeHautNiveau() {
        return artisteDeHautNiveau;
    }

    public void setArtisteDeHautNiveau(String artisteDeHautNiveau) {
        this.artisteDeHautNiveau = artisteDeHautNiveau;
    }

    public String getArtisteDeHautNiveauCode() {
        return artisteDeHautNiveauCode;
    }

    public void setArtisteDeHautNiveauCode(String artisteDeHautNiveauCode) {
        this.artisteDeHautNiveauCode = artisteDeHautNiveauCode;
    }

    public String getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(String dateInscription) {
        this.dateInscription = dateInscription;
    }

    public String getDateDImpression() {
        return dateDImpression;
    }

    public void setDateDImpression(String dateDImpression) {
        this.dateDImpression = dateDImpression;
    }

    public String getFicheImprimeeON() {
        return ficheImprimeeON;
    }

    public void setFicheImprimeeON(String ficheImprimeeON) {
        this.ficheImprimeeON = ficheImprimeeON;
    }

    public String getFicheImprimeeCode() {
        return ficheImprimeeCode;
    }

    public void setFicheImprimeeCode(String ficheImprimeeCode) {
        this.ficheImprimeeCode = ficheImprimeeCode;
    }

    public String getEligibleàLaPolitiqueDeLaVille() {
        return eligibleàLaPolitiqueDeLaVille;
    }

    public void setEligibleàLaPolitiqueDeLaVille(String eligibleàLaPolitiqueDeLaVille) {
        this.eligibleàLaPolitiqueDeLaVille = eligibleàLaPolitiqueDeLaVille;
    }

    public String getFicheDeReorientation() {
        return ficheDeReorientation;
    }

    public void setFicheDeReorientation(String ficheDeReorientation) {
        this.ficheDeReorientation = ficheDeReorientation;
    }

    public String getFicheDeReorientationCode() {
        return ficheDeReorientationCode;
    }

    public void setFicheDeReorientationCode(String ficheDeReorientationCode) {
        this.ficheDeReorientationCode = ficheDeReorientationCode;
    }

    public String getDossierPapier() {
        return dossierPapier;
    }

    public void setDossierPapier(String dossierPapier) {
        this.dossierPapier = dossierPapier;
    }

    public String getDossierPapierCode() {
        return dossierPapierCode;
    }

    public void setDossierPapierCode(String dossierPapierCode) {
        this.dossierPapierCode = dossierPapierCode;
    }

    public String getAnneeDEntreeEnSeconde() {
        return anneeDEntreeEnSeconde;
    }

    public void setAnneeDEntreeEnSeconde(String anneeDEntreeEnSeconde) {
        this.anneeDEntreeEnSeconde = anneeDEntreeEnSeconde;
    }

    public String getDossierDematerialiseCode() {
        return dossierDematerialiseCode;
    }

    public void setDossierDematerialiseCode(String dossierDematerialiseCode) {
        this.dossierDematerialiseCode = dossierDematerialiseCode;
    }

    public String getDossierDematerialise() {
        return dossierDematerialise;
    }

    public void setDossierDematerialise(String dossierDematerialise) {
        this.dossierDematerialise = dossierDematerialise;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CandidatHistory getCandidatHistory() {
        return candidatHistory;
    }

    public void setCandidatHistory(CandidatHistory candidatHistory) {
        this.candidatHistory = candidatHistory;
    }
}
