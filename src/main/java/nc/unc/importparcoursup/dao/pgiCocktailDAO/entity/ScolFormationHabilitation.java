package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationHabilitation.TABLE_NAME)
public class ScolFormationHabilitation {
    public final static String TABLE_NAME = "SCOLARITE.SCOL_FORMATION_HABILITATION";
    @Id
    private Long FHAB_KEY;
    @Column(name = "FSPN_KEY")
    private Long fspnkey;
    @Column(name = "FHAB_NIVEAU")
    private int fhabniveau;
    @Column(name = "FHAB_OUVERT")
    private String fhabouvert;
    @Column(name = "FANN_KEY")
    private int fannkey;

    public Long getFHAB_KEY() {
        return FHAB_KEY;
    }

    public void setFHAB_KEY(Long FHAB_KEY) {
        this.FHAB_KEY = FHAB_KEY;
    }

    public Long getFspnkey() {
        return fspnkey;
    }

    public void setFspnkey(Long fspnkey) {
        this.fspnkey = fspnkey;
    }

    public int getFhabniveau() {
        return fhabniveau;
    }

    public void setFhabniveau(int fhabniveau) {
        this.fhabniveau = fhabniveau;
    }

    public String getFhabouvert() {
        return fhabouvert;
    }

    public void setFhabouvert(String fhabouvert) {
        this.fhabouvert = fhabouvert;
    }

    public int getFannkey() {
        return fannkey;
    }

    public void setFannkey(int fannkey) {
        this.fannkey = fannkey;
    }
}
