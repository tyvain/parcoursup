package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.Bac;
import org.springframework.data.repository.CrudRepository;

public interface BacRepository extends CrudRepository<Bac, Long> {
    boolean existsByBaccode(String baccode);
}
