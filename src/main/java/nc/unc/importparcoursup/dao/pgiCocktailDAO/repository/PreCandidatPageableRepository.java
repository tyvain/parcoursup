package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PreCandidatPageableRepository extends PagingAndSortingRepository<PreCandidat, Long> {
    Page<PreCandidat> findAll(Pageable pageable);
}
