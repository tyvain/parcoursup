package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationSpecialisation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ScolFormationSpecialisationRepository extends CrudRepository<ScolFormationSpecialisation, Long> {
    @Query("SELECT sfs " +
            "FROM ScolFormationDiplome sfd, ScolFormationSpecialisation sfs, ScolFormationHabilitation sfh " +
            "WHERE 1=1 " +
            "and sfs.fdipcode=sfd.fdipcode " +
            "and sfh.fspnkey=sfs.fspnkey " +
            "and sfd.fdipcode =:fdipcode " +
            "and sfh.fannkey=:year " +
            "and sfh.fhabniveau=1 " +
            "and sfh.fhabouvert='O' ")
    Optional<ScolFormationSpecialisation> findByFdipCode(@Param("fdipcode")String fdipcode, @Param("year")int year);
    //ScolFormationSpecialisation findByFdipcode(String fdipcode);
}
