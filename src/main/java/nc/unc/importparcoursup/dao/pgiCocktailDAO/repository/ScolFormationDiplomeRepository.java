package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.ScolFormationDiplome;
import org.springframework.data.repository.CrudRepository;

public interface ScolFormationDiplomeRepository extends CrudRepository<ScolFormationDiplome, Long> {
}
