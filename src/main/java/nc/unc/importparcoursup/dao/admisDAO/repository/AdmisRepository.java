package nc.unc.importparcoursup.dao.admisDAO.repository;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AdmisRepository extends CrudRepository<Admis, Long> {
    List<Admis> findAllAdmisByAdmisHistory(AdmisHistory admisHistory);
}
