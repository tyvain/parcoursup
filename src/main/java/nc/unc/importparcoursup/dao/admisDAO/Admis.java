package nc.unc.importparcoursup.dao.admisDAO;

import com.opencsv.bean.CsvBindByPosition;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = Admis.TABLE_NAME)
public class Admis {
    public final static String TABLE_NAME = "ADMIS";
    public final static String ADMIS_HISTORY_ID = "ADMIS_HISTORY_ID";
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne
    @JoinColumn(name = ADMIS_HISTORY_ID)
    private AdmisHistory admisHistory;
    @OneToMany(mappedBy = "admis", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<AdmisRejet> admisRejet;
    @CsvBindByPosition(position = 0)
    private String candNumero;
    @CsvBindByPosition(position = 1)
    private String candBea;
    @CsvBindByPosition(position = 2)
    private String candCivilite;
    @CsvBindByPosition(position = 3)
    private String candNom;
    @CsvBindByPosition(position = 4)
    private String candPrenom;
    @CsvBindByPosition(position = 5)
    private String candPrenom2;
    @CsvBindByPosition(position = 6)
    private String candDateNais;
    @CsvBindByPosition(position = 7)
    private String candComnais;
    @CsvBindByPosition(position = 8)
    private String candComnaisCode;
    @CsvBindByPosition(position = 9)
    private String dptgCodeNais;
    @CsvBindByPosition(position = 10)
    private String paysCodeNais;
    @CsvBindByPosition(position = 11)
    private String natOrdre;
    @CsvBindByPosition(position = 12)
    private String candAdr1Parent;
    @CsvBindByPosition(position = 13)
    private String candAdr2Parent;
    @CsvBindByPosition(position = 14)
    private String candCpParent;
    @CsvBindByPosition(position = 15)
    private String candVilleParent;
    @CsvBindByPosition(position = 16)
    private String candVilleParentCode;
    @CsvBindByPosition(position = 17)
    private String candPaysParent;
    @CsvBindByPosition(position = 18)
    private String candTelParent;
    @CsvBindByPosition(position = 19)
    private String candPortScol;
    @CsvBindByPosition(position = 20)
    private String candEmailScol;
    @CsvBindByPosition(position = 21)
    private String bacCode;
    @CsvBindByPosition(position = 22)
    private String candAnbac;
    @CsvBindByPosition(position = 23)
    // entrée dans rne.c_rne a verifier
    private String etabCodeBac;
    @CsvBindByPosition(position = 24)
    private String etabLibelleBac;
    @CsvBindByPosition(position = 25)
    private String candVilleBac;
    @CsvBindByPosition(position = 26)
    private String dptgEtabBac;
    @CsvBindByPosition(position = 27)
    private String paysEtabBac;
    @CsvBindByPosition(position = 28)
    private String histEnsDerEtab;
    @CsvBindByPosition(position = 29)
    // entrée dans rne.c_rne a verifier
    private String etabCodeDerEtab;
    @CsvBindByPosition(position = 30)
    private String histLibelleDerEtab;
    @CsvBindByPosition(position = 31)
    private String histVilleDerEtab;
    @CsvBindByPosition(position = 32)
    private String dptgCodeDerEtab;
    @CsvBindByPosition(position = 33)
    private String paysCodeDerEtab;
    @CsvBindByPosition(position = 34)
    private String candEtabChoix;
    @CsvBindByPosition(position = 35)
    private String candFormationChoix;

    @Transient
    // formation diplome code
    public String getFDipCode() {
        if (StringUtils.contains(candFormationChoix, ";")) {
            return StringUtils.split(candFormationChoix, ";")[0];
        }
        return candFormationChoix;
    }

    @Transient
    // formation specialisation clef
    // PRE_CANDIDATURE.CAND_DSPE_CODE
    /**
     * lire scolarite.scol_formation_specialisation.fspnkey
     */
    public String getFSpnKey() {
        if (StringUtils.contains(candFormationChoix, ";")) {
            return StringUtils.split(candFormationChoix, ";")[1];
        }
        return "";
    }

    public Admis() {
        // default constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdmisHistory getAdmisHistory() {
        return admisHistory;
    }

    public void setAdmisHistory(AdmisHistory admisHistory) {
        this.admisHistory = admisHistory;
    }

    public String getCandNumero() {
        return candNumero;
    }

    public void setCandNumero(String candNumero) {
        this.candNumero = candNumero;
    }

    public String getCandBea() {
        return candBea;
    }

    public void setCandBea(String candBea) {
        this.candBea = candBea;
    }

    public String getCandCivilite() {
        return candCivilite;
    }

    public void setCandCivilite(String candCivilite) {
        this.candCivilite = candCivilite;
    }

    public String getCandNom() {
        return candNom;
    }

    public void setCandNom(String candNom) {
        this.candNom = candNom;
    }

    public String getCandPrenom() {
        return candPrenom;
    }

    public void setCandPrenom(String candPrenom) {
        this.candPrenom = candPrenom;
    }

    public String getCandPrenom2() {
        return candPrenom2;
    }

    public void setCandPrenom2(String candPrenom2) {
        this.candPrenom2 = candPrenom2;
    }

    public String getCandDateNais() {
        return candDateNais;
    }

    public void setCandDateNais(String candDateNais) {
        this.candDateNais = candDateNais;
    }

    public String getCandComnais() {
        return candComnais;
    }

    public void setCandComnais(String candComnais) {
        this.candComnais = candComnais;
    }

    public String getCandComnaisCode() {
        return candComnaisCode;
    }

    public void setCandComnaisCode(String candComnaisCode) {
        this.candComnaisCode = candComnaisCode;
    }

    public String getDptgCodeNais() {
        return dptgCodeNais;
    }

    public void setDptgCodeNais(String dptgCodeNais) {
        this.dptgCodeNais = dptgCodeNais;
    }

    public String getPaysCodeNais() {
        return paysCodeNais;
    }

    public void setPaysCodeNais(String paysCodeNais) {
        this.paysCodeNais = paysCodeNais;
    }

    public String getNatOrdre() {
        return natOrdre;
    }

    public void setNatOrdre(String natOrdre) {
        this.natOrdre = natOrdre;
    }

    public String getCandAdr1Parent() {
        return candAdr1Parent;
    }

    public void setCandAdr1Parent(String candAdr1Parent) {
        this.candAdr1Parent = candAdr1Parent;
    }

    public String getCandAdr2Parent() {
        return candAdr2Parent;
    }

    public void setCandAdr2Parent(String candAdr2Parent) {
        this.candAdr2Parent = candAdr2Parent;
    }

    public String getCandCpParent() {
        return candCpParent;
    }

    public void setCandCpParent(String candCpParent) {
        this.candCpParent = candCpParent;
    }

    public String getCandVilleParent() {
        return candVilleParent;
    }

    public void setCandVilleParent(String candVilleParent) {
        this.candVilleParent = candVilleParent;
    }

    public String getCandVilleParentCode() {
        return candVilleParentCode;
    }

    public void setCandVilleParentCode(String candVilleParentCode) {
        this.candVilleParentCode = candVilleParentCode;
    }

    public String getCandPaysParent() {
        return candPaysParent;
    }

    public void setCandPaysParent(String candPaysParent) {
        this.candPaysParent = candPaysParent;
    }

    public String getCandTelParent() {
        return candTelParent;
    }

    public void setCandTelParent(String candTelParent) {
        this.candTelParent = candTelParent;
    }

    public String getCandPortScol() {
        return candPortScol;
    }

    public void setCandPortScol(String candPortScol) {
        this.candPortScol = candPortScol;
    }

    public String getCandEmailScol() {
        return candEmailScol;
    }

    public void setCandEmailScol(String candEmailScol) {
        this.candEmailScol = candEmailScol;
    }

    public String getBacCode() {
        return bacCode;
    }

    public void setBacCode(String bacCode) {
        this.bacCode = bacCode;
    }

    public String getCandAnbac() {
        return candAnbac;
    }

    public void setCandAnbac(String candAnbac) {
        this.candAnbac = candAnbac;
    }

    public String getEtabCodeBac() {
        return etabCodeBac;
    }

    public void setEtabCodeBac(String etabCodeBac) {
        this.etabCodeBac = etabCodeBac;
    }

    public String getEtabLibelleBac() {
        return etabLibelleBac;
    }

    public void setEtabLibelleBac(String etabLibelleBac) {
        this.etabLibelleBac = etabLibelleBac;
    }

    public String getCandVilleBac() {
        return candVilleBac;
    }

    public void setCandVilleBac(String candVilleBac) {
        this.candVilleBac = candVilleBac;
    }

    public String getDptgEtabBac() {
        return dptgEtabBac;
    }

    public void setDptgEtabBac(String dptgEtabBac) {
        this.dptgEtabBac = dptgEtabBac;
    }

    public String getPaysEtabBac() {
        return paysEtabBac;
    }

    public void setPaysEtabBac(String paysEtabBac) {
        this.paysEtabBac = paysEtabBac;
    }

    public String getHistEnsDerEtab() {
        return histEnsDerEtab;
    }

    public void setHistEnsDerEtab(String histEnsDerEtab) {
        this.histEnsDerEtab = histEnsDerEtab;
    }

    public String getEtabCodeDerEtab() {
        return etabCodeDerEtab;
    }

    public void setEtabCodeDerEtab(String etabCodeDerEtab) {
        this.etabCodeDerEtab = etabCodeDerEtab;
    }

    public String getHistLibelleDerEtab() {
        return histLibelleDerEtab;
    }

    public void setHistLibelleDerEtab(String histLibelleDerEtab) {
        this.histLibelleDerEtab = histLibelleDerEtab;
    }

    public String getHistVilleDerEtab() {
        return histVilleDerEtab;
    }

    public void setHistVilleDerEtab(String histVilleDerEtab) {
        this.histVilleDerEtab = histVilleDerEtab;
    }

    public String getDptgCodeDerEtab() {
        return dptgCodeDerEtab;
    }

    public void setDptgCodeDerEtab(String dptgCodeDerEtab) {
        this.dptgCodeDerEtab = dptgCodeDerEtab;
    }

    public String getPaysCodeDerEtab() {
        return paysCodeDerEtab;
    }

    public void setPaysCodeDerEtab(String paysCodeDerEtab) {
        this.paysCodeDerEtab = paysCodeDerEtab;
    }

    public String getCandEtabChoix() {
        return candEtabChoix;
    }

    public void setCandEtabChoix(String candEtabChoix) {
        this.candEtabChoix = candEtabChoix;
    }

    public String getCandFormationChoix() {
        return candFormationChoix;
    }

    public void setCandFormationChoix(String candFormationChoix) {
        this.candFormationChoix = candFormationChoix;
    }

    public List<AdmisRejet> getAdmisRejet() {
        return admisRejet;
    }

    public void setAdmisRejet(List<AdmisRejet> admisRejet) {
        this.admisRejet = admisRejet;
    }
}
