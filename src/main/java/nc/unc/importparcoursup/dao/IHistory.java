package nc.unc.importparcoursup.dao;

import java.util.Date;

public interface IHistory {
    Date getJobExecutionDate();
}
