package nc.unc.importparcoursup.view.verificationrules;

import nc.unc.importparcoursup.dao.IHistory;
import nc.unc.importparcoursup.dao.IHistoryRepository;
import nc.unc.importparcoursup.model.UAI;
import nc.unc.importparcoursup.utils.MyFileUtils;
import nc.unc.importparcoursup.view.components.UploadComponent;

import java.util.Date;

public class DateAfterLastUploadForSameUAI extends CheckElement {
    private static final long serialVersionUID = 1L;

    public DateAfterLastUploadForSameUAI(UploadComponent upload, IHistoryRepository historyRepository) {
        super(upload, historyRepository);
    }

    @Override
    public boolean isValidCheck() {
        Date fileDate = MyFileUtils.getDateFromFileName(getFileName());
        IHistory history = getHistoryForSameUAI();
        return history == null || fileDate.after(history.getJobExecutionDate());
    }

    private IHistory getHistoryForSameUAI() {
        UAI uai = MyFileUtils.getUAIFromFileName(getFileName());
        return historyRepository.findTopByUaiOrderByJobExecutionDateDesc(uai);
    }

    @Override
    protected String getLabel() {
        return "Date > date dernière éxecution";
    }

}
