package nc.unc.importparcoursup.view.verificationrules;

import nc.unc.importparcoursup.utils.MyFileUtils;
import nc.unc.importparcoursup.view.components.UploadComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileNameIsCorrect extends CheckElement {
    private static final Logger log = LoggerFactory.getLogger(FileNameIsCorrect.class);
    private static final long serialVersionUID = 1L;

    public FileNameIsCorrect(UploadComponent upload) {
        super(upload);
    }

    @Override
    public boolean isValidCheck() {
        try {
            MyFileUtils.checkFileName(uploadComponent.getUploadedFileName());
        } catch (Exception e) {
            log.error("Wrong file name: " + e);
            return false;
        }
        return true;
    }

    @Override
    protected String getLabel() {
        return "Nom du fichier";
    }

}
