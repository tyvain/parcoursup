package nc.unc.importparcoursup.view.informationrules;

import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;

public class LastExecNbRejetIE extends InformationElement<AdmisHistory> {
    private static final long serialVersionUID = 1L;
    AdmisRejetRepository admisRejetRepository;

    public LastExecNbRejetIE(AdmisRejetRepository admisRejetRepository) {
        this.admisRejetRepository = admisRejetRepository;
    }

    @Override
    public String updateValue(AdmisHistory item) {
        return "" + admisRejetRepository.findAdmisRejetByAdmisHistory(item).size();
    }

    @Override
    protected String getLabel() {
        return "Dernier Job - Rejets";
    }
}
