package nc.unc.importparcoursup.view.components;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisHistoryRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRejetRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.view.informationrules.*;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class InformationComponent extends VerticalLayout {
    private static final long serialVersionUID = 1L;
    AdmisHistoryRepository admisHistoryRepository;
    AdmisRepository admisRepository;
    AdmisRejetRepository admisRejetRepository;
    UploadComponent myUploadComponent;
    List<InformationElement<File>> fileCheck;
    List<InformationElement<AdmisHistory>> admisRepodCheck;

    public InformationComponent(AdmisHistoryRepository admisHistoryRepository,
                                UploadComponent myUploadComponent,
                                AdmisRepository admisRepository,
                                AdmisRejetRepository admisRejetRepository) {
        this.admisHistoryRepository = admisHistoryRepository;
        this.myUploadComponent = myUploadComponent;
        this.admisRepository = admisRepository;
        this.admisRejetRepository = admisRejetRepository;
        this.getStyle().set("border", "1px solid");
        this.setHeight("100%");
        this.setWidth("100%");
        add(new Label("Informations"));
        fileCheck = List.of(new NbLinesInFileIE(), new NbColumnInFileIE());
        admisRepodCheck = List.of(new LastExecDateIE(), new LastExecNbLinesIE(), new LastExecNbRejetIE(admisRejetRepository));
        fileCheck.forEach(this::addComponent);
        admisRepodCheck.forEach(this::addComponent);
    }

    private void addComponent(@SuppressWarnings("rawtypes") InformationElement element) {
        element.setSizeFull();
        add(element);
    }

    public void update() {
        fileCheck.forEach(element -> element.update(myUploadComponent.getFile()));
        admisRepodCheck.forEach(element -> element
                .update(Optional.ofNullable(admisHistoryRepository.findTopByOrderByJobExecutionDateDesc())));
    }
}
