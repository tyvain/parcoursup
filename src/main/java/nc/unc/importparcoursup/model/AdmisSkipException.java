package nc.unc.importparcoursup.model;

import nc.unc.importparcoursup.dao.admisDAO.Admis;

public class AdmisSkipException extends Exception {
    private TypeRejet typeRejet;
    private Admis admis;
    private String information;

    public AdmisSkipException(TypeRejet typeRejet, Admis admis) {
        super();
        this.typeRejet = typeRejet;
        this.admis = admis;
    }

    public AdmisSkipException(TypeRejet typeRejet, Admis admis, String information) {
        super();
        this.typeRejet = typeRejet;
        this.admis = admis;
        this.information = information;
    }

    public TypeRejet getTypeRejet() {
        return typeRejet;
    }

    public Admis getAdmis() {
        return admis;
    }

    public String getInformation() {
        return information;
    }
}
